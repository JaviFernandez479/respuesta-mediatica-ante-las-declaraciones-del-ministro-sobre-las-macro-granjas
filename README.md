# Respuesta mediática ante las declaraciones del ministro sobre las macro granjas

En este proyecto se ha estudiado la reacción de la población ante las declaraciones que hizo el ministro de consumo, Alberto Garzón, sobre la industria cárnica española.

### Objetivo de este proyecto

Hace unos días el ministro Alberto Garzón calificó la industria cárnica española como "de mala calidad". Las razones que ha dado para hacer esas declaraciones han sido basadas en el uso de macrogranjas y malas condiciones para los animales, lo que no le ha sentado nada bien a los ganaderos ni a los dueños de las empresas carniceras.

La ganadería es un sector muy importante en España y los proveedores de estos alimentos han sido puestos "en tela de juicio", según las propias palabras de los afectados.

Sin embargo, hay muchas otras personas que defienden el punto de vista del ministro y que lo apoyan, lo que ha generado revuelo en Twitter estas últimas semanas.

El objetivo de este trabajo es el de capturar tweets relacionados con el tema buscando unas palabras clave e identificar si los comentarios son positivos o negativos, para saber cuál es la opinión general de las personas.

### Tecnologías utilizadas

Para llevar a cabo este proyecto hemos utilizado varias tecnología:

    - Cuenta de desarrollador de Twitter, para acceder a la API.
    - Eclipse IDE como entorno de programación.
    - Prácticas de clase programadas en Java para capturar los tweets en Streaming.
    - API de Monkeylearn para clasificar los tweets entre positivos y negativos.
    - Power BI para análisis y representación de los datos.

### Desarrollo del proyecto

Primero capturamos los tweets utilizando la práctica de captura de tweets en Streaming programada en Java durante el desarrollo de la asignatura.
Después se reprocesan eliminando los emojis y los retornos de carro.
Las palabras clave utilizadas para filtrar los tweets han sido:

    - Garzón
    - Garzon
    - garzon 
    - garzón
    - Macrogranjas
    - Ganadería 
    - Ganaderia

A medida que van llegando los tweets se van clasificando con el Sentiment Analyisis de la API de Monkeylearn. 
Una vez clasificados, se guardan en un archivo csv donde nos da el porcentaje y el número de tweets positivos, negativos o neutros. 
Después hemos utilizado ese archivo csv para sacar las conclusiones finales.
Para obtener la representación de las gráficas hemos utilizado el archivo csv y la herramienta Power BI.

### Conclusión

Como podemos observar en el gráfico circular, podemos observar que hay un mayor número de comentarios negativos que positivos, sin embargo, la mayoría de los comentarios han sido clasificados como neutros. Esto probablemente se deba a que la librería Monkeylearn es de uso libre y por lo tanto no tendrá una efectividad demasiado alta. Dicho esto, combinando la búsqueda de palabras clave con el filtrado por hashtags y realizando pruebas de manera regular el resultado podría acercarse más a la realidad de lo que se acerca este proyecto académico.

